import { request } from './http';
// import uuid4 from 'uuid/v4';

const jwtStorageKey = 'basketAuthJWT';
const guestIdStorageKey = 'basketAuthGuestId';

interface IJwt {
    token: string,
    expirationTime: number
}

export const getJwt = (): null | IJwt => {
    const jwt = localStorage.getItem(jwtStorageKey);
    
    if(jwt) {
        return JSON.parse(jwt);
    }
    
    return null;
};

export const setJwt = (jwt: IJwt) => {
    localStorage.setItem(jwtStorageKey, JSON.stringify(jwt));
};

export const removeJwt = () => {
    localStorage.removeItem(jwtStorageKey);
};

export const getGuestId = (): null | string => {
    return localStorage.getItem(guestIdStorageKey);
};

export const setGuestId = (uuid: string) => {
    localStorage.setItem(guestIdStorageKey, uuid);
};

/** @todo save JWT */
export const getTokenForGuest = (uuid: string) => {
    return request(
        'tokens/guest',
        'POST',
        { customerId: uuid }
    );
};

/** @todo save JWT */
export const getTokenForCustomer = (login: string, password: string) => {
    return request(
        'tokens/login',
        'POST',
        { login, password }
    );
};