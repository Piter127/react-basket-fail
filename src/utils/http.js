import { apiUrl, baseUrl } from 'config';
import { getJwt } from './jwt';
import { ajax } from 'rxjs/ajax';

const defaultHeaders = {
    'Content-Type': 'application/json'
};

/**
 * @todo get @var token
 */
const getAuthHeaders = (url: string) => {
    const jwt = getJwt();
    const token = jwt ? jwt.token : '';
    
    return { Authorization: (url.includes('token')) ? undefined : `Bearer ${token}` };
};

export const request = (url: string, method: string = 'GET', data?: any) => {
    url = `${baseUrl}${apiUrl}${url}`;
    const body = data || {};
    
    const headers = {
        ...defaultHeaders,
        ...getAuthHeaders(url)
    };
    
    return ajax({ url, method, body, headers });
};